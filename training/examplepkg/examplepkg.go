package examplepkg

import "fmt"

type Person struct {
	name string
	age  int
}
type datatype int

func init() {
	fmt.Println("At example pkg")
}
func PrintString(a int, num1 ...string) {
	fmt.Println(num1)
	var p Person
	p = p.PrintString()
	fmt.Println(p)
}
func init() {
	fmt.Println("At example pkg2")
}
func (p Person) PrintString() Person {
	p.name = "Renu"
	p.age = 12
	return p
}
func (v1 datatype) PrintString() {

}
