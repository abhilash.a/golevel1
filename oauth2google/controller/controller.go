package controller

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"oauth2google/config"
)

func GoogleLogin(w http.ResponseWriter, r *http.Request) {
	googleConfig := config.SetupConfig()
	url := googleConfig.AuthCodeURL("randomstate") //url of a gmail sign in page
	http.Redirect(w, r, url, http.StatusSeeOther)
}

func GoogleCallback(w http.ResponseWriter, r *http.Request) {
	state := r.URL.Query()["state"][0]
	if state != "randomstate" {
		fmt.Fprintln(w, "state doenst match")
		return
	}

	code := r.URL.Query()["code"][0]
	googleConfig := config.SetupConfig()
	token, err := googleConfig.Exchange(context.Background(), code)
	fmt.Println(token)
	fmt.Fprintln(w, token, "user data fetched failed")

	if err != nil {
		fmt.Fprintln(w, "code token exchange failaed")
	}
	//to fetch user info from api
	resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	fmt.Println(resp)
	if err != nil {
		fmt.Fprintln(w, "user data fetched failed")

	}
	userData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Fprintln(w, "jsson parsing failed")
	}
	fmt.Fprintln(w, string(userData))
	fmt.Println(string(userData))
	defer resp.Body.Close()

}
