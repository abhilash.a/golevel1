module oauth2google

go 1.18

require golang.org/x/oauth2 v0.0.0-20220822191816-0ebed06d0094

require (
	cloud.google.com/go/compute v1.9.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.0.0-20220826154423-83b083e8dc8b // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
