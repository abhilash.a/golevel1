package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type User struct {
	Id    int    `form:"id"`
	Name  string `form:"name"`
	Email string `form:"email"`
} //form is used user passing as queryyparam and GETreqest

//binding data from body json  passed by user using verb-POST
func main() {
	r := gin.Default()
	r.POST("/user", func(c *gin.Context) {
		var user User
		if err := c.ShouldBindJSON(&user); err != nil { //binding user passed body  payload json to struct model object vaiable
			fmt.Printf("%+v", err)
		} else {
			fmt.Printf(" userobj %+v", user)
		}
		c.JSON(http.StatusCreated, gin.H{
			"code":     http.StatusCreated,
			"userData": user,
		})
	})
	r.Run(":8080")
}
