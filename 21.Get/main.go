package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

func main() {
	// getRequest()
	// postRequest()
	postFormRequest()
}

// func getRequest() {
// 	const myurl = "http://localhost:8000/get"
// 	response, err := http.Get(myurl)
// 	if err != nil {
// 		panic(err)
// 	}
// 	var contentString strings.Builder
// 	content, _ := ioutil.ReadAll(response.Body)
// 	defer response.Body.Close()
// 	countContent, _ := contentString.Write(content)
// 	fmt.Println("count", countContent)
// 	fmt.Println("body is", contentString.String())

// }

// func postRequest() {
// 	const myurl = "http://localhost:8000/post"
// 	stringBody := strings.NewReader(`
// 	{
// 	"name":"abhilash",
// 	"class":"ten"	}`) //fake payload measn data to get posted
// 	response, err := http.Post(myurl, "application/json", stringBody)
// 	if err != nil {
// 		panic(err)
// 	}
// 	var contentString strings.Builder
// 	content, _ := ioutil.ReadAll(response.Body)
// 	defer response.Body.Close()
// 	countContent, _ := contentString.Write(content)
// 	fmt.Println("count", countContent)
// 	fmt.Println("body is", contentString.String())
// 	// fmt.Println(string(content))
// }
func postFormRequest() {
	myurl := "http://localhost:8000/postform" // is used when image to be enclosed and form data need to be encoded
	data := url.Values{}                      //keyvalue pair that we gibe in encoded part in thunder vpn
	data.Add("firstname", "abhi")
	data.Add("lastname", "lash")
	response, err := http.PostForm(myurl, data)
	if err != nil {
		panic(err)
	}

	content, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	var stringBuilderofContent strings.Builder
	ContentLength, _ := stringBuilderofContent.Write(content)
	fmt.Println(ContentLength)
	fmt.Println(stringBuilderofContent.String())

}
