package db

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

// const (
// 	host     = "localhost"
// 	port     = 5432
// 	user     = "postgres"
// 	password = "tech@123"
// 	dbname   = "db1"
// )

type Env struct {
	Host     string
	Port     int
	User     string
	Password string
	DBName   string
}

type Db struct {
	Dbb  *sql.DB
	Envv *Env
}

// type Err struct {
// 	Status int
// 	err    error
// }

var dbValue = &Db{Dbb: nil, Envv: nil}

func LoadVariable(host, user, password, dbname string, port int) {
	dbValue.Envv = &Env{
		Host:     host,
		Port:     port,
		User:     user,
		Password: password,
		DBName:   dbname,
	}

}
func DbConnection() *Db {

	// connection string
	if dbValue.Envv == nil {
		fmt.Println("envv nil")
	}
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", dbValue.Envv.Host, dbValue.Envv.Port, dbValue.Envv.User, dbValue.Envv.Password, dbValue.Envv.DBName)

	// open database
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		fmt.Println("db connection eror")
	}
	dbValue.Dbb = db
	// fmt.Println("entharu", db)
	// CheckError(err)

	// // result, _ := db.Query(`select * from helootable`)

	// var id int
	// for result.Next() {

	// 	_ = result.Scan(&id)

	// }
	// defer result.Close()
	// fmt.Println("val", id)
	// close database

	// check db
	err = db.Ping()
	CheckError(err)

	fmt.Println("Connected!")
	return dbValue

}

func CheckError(err error) error {
	if err != nil {
		panic(err)
	}
	return err
}

func Query() {
	db := DbConnection()
	dbc, _ := db.Dbb.Exec("select * from helootable")
	fmt.Println(dbc)
}
