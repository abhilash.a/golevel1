package main

import (
	"addictive2/config"
	"addictive2/db"
	"fmt"
	"os"
	"strconv"
)

func main() {
	port, _ := strconv.Atoi(os.Getenv("ADDICTIVE_PORT"))

	fmt.Println(config.Load("APPNAME"))
	db.LoadVariable(os.Getenv("ADDICTIVE_HOST"), os.Getenv("ADDICTIVE_USER"), os.Getenv("ADDICTIVE_PASSWORD"), os.Getenv("ADDICTIVE_DBNAME"), port)
	db.DbConnection()
	db.Query()

}
