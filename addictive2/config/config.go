package config

import "os"

func Load(key string) string {
	keyExportedOnTErminal := os.Getenv(key) //key exported on terminal
	return keyExportedOnTErminal
}
