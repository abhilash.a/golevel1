## Basic - Go Programing fundamentals : Level-1

Write Go Programs for following questions:

1 Print Hello world
2 Programm to add two numbers
3 Define variable with var and short declaration method (Rewrite the question3 with shor variable declaration)
4 Find Sum and average of three numbers
5 Simple interest

        Simple Interest = (principal amount * rate of interest * years) / 100

6 Find Compound Interest

    Write a Go Program to calculate Compound Interest. This golang program allows the user to enter the principal amount, totals years, and interest rates and then find the Compound Interest.

        Future Compound Interest =  principal amount * (1 + interest rates)years
        Compound Interest =  Future Compound Interest – principal amount

7 Find area of circle

        A = π r²

8 Program to add, substract,multiply and divide two float numbers.
9 Find Cube of a number
10 Program to check a number is Even or Odd
11 Find whether a given year is a leap year or not
12 Swap two numbers
13 Calculate power of a number
14 Sum of Digits of a number
15 Reverse of a number
16 Find smallest and largest of three numbers
17 Program to check a number is prime number or not
18 Write a C program to find the eligibility of admission for a professional course based on the following criteria:
Eligibility Criteria : Marks in Maths >=65 and Marks in Phy >=55 and Marks in Chem>=50 and Total in all three subject >=190 or Total in Maths and Physics >=140
Input the marks obtained in Physics :65, Input the marks obtained in Chemistry :51, Input the marks obtained in Mathematics :72, Total marks of Maths, Physics and Chemistry : 188, Total marks of Maths and Physics : 137
The candidate is not eligible.
19 Print corresponding day in a week
20 Program to create a simple calculator
