package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

//model for courses - go into a file

type Course struct {
	Courseid    string  `json:"courseid"`
	Coursename  string  `json:"coursename"`
	Courseprice int     `json:"courseprice"`
	Author      *Author `json:"author"` //using type that we are created somewhere
}

type Author struct {
	Fullname string `json:"fullname"`
	Website  string `json:"website"`
}

var courses []Course //creating slices of type course struct    fake db
//middleware or helper or helper method  -go into a file
func (c *Course) isEmpty() bool { // recieving the type of course into function for accessing field and validating
	//return c.Courseid == "" && c.Coursename == ""
	return c.Coursename == "" //post operation course id is created using rand.seed mdthod and convert it in to string
}

func main() {
	//access router to connect oursysyetm sever to routwr
	r := mux.NewRouter()

	//seeding data in to created model
	courses = append(courses, Course{Courseid: "2", Coursename: "javascript", Courseprice: 199, Author: &Author{Fullname: "hithesh", Website: "learncode.online"}})
	courses = append(courses, Course{Courseid: "4", Coursename: "java", Courseprice: 299, Author: &Author{Fullname: "Telusko", Website: "Telusko.online"}})

	//handlingcontroller using router

	r.HandleFunc("/", serveHome).Methods("GET")
	r.HandleFunc("/courses", getAllCourses).Methods("GET")
	r.HandleFunc("/course/{id}", getOneCourses).Methods("GET")
	r.HandleFunc("/course", createOneCourse).Methods("POST")
	r.HandleFunc("/course/{id}", oneCourseupdate).Methods("PUT")
	r.HandleFunc("/course/{id}", deleteonecourse).Methods("DELETE")
	r.HandleFunc("/course/", deleteallcourse).Methods("DELETE")

	// to serve it in a port
	log.Fatal(http.ListenAndServe(":4000", r))

}

//controller- in to file

//serveroute, actually as a developer serving data to front end  i.e is the controller  part

func serveHome(w http.ResponseWriter, r *http.Request) {
	fmt.Println("home   /")
	w.Write([]byte("<h1>hi welcome to code.online</h1>"))
}
func getAllCourses(w http.ResponseWriter, r *http.Request) {
	fmt.Println("get all courses")
	w.Header().Set("Content-Type", "application/json") //keyvalue pairs content type in response header
	json.NewEncoder(w).Encode(courses)                 //need to enocde json to avoid json visibility
}
func getOneCourses(w http.ResponseWriter, r *http.Request) {
	fmt.Println("to get deltails of one course with id")
	w.Header().Set("Content-Type", "application/json")

	//get take parameters of request requested by the client
	params := mux.Vars(r)
	//loop courses//find which id in courses //then return that id as response
	fmt.Printf("%T \n%v", params, params)
	idd := params["id"]
	for _, course := range courses {
		if course.Courseid == params["id"] {
			json.NewEncoder(w).Encode(course)
			return
		}
		json.NewEncoder(w).Encode(idd + ":id not found in fake db")
		return
	}

}

// above all function we are making json and send it to front end, so encode is required
//but this function post,, json data is coming from front end that is user,so we want to decode data

func createOneCourse(w http.ResponseWriter, r *http.Request) {
	fmt.Println("adding id in to courses slice")
	w.Header().Set("Content-Type", "application/json")

	var course Course                           //destructure  structre using decode here,another method loop through
	_ = json.NewDecoder(r.Body).Decode(&course) //reference of course , Decode return err avoid err para

	// what if body is empty
	if r.Body == nil {
		json.NewEncoder(w).Encode("data given by user not found,please send some data")

	}

	//check what if passing {}

	if course.isEmpty() {
		json.NewEncoder(w).Encode("no data in inputed json {}")
		return

	}
	for _, coursee := range courses {
		if course.Coursename == coursee.Coursename {
			json.NewEncoder(w).Encode("course name already exists")
			return
		}

	}

	//generate unique course  id , nd convert it in to string

	rand.Seed(time.Now().UnixNano())
	course.Courseid = strconv.Itoa(rand.Intn(100)) //rand.intn return int convert it into string
	courses = append(courses, course)
	json.NewEncoder(w).Encode(course)
	return

}

func oneCourseupdate(w http.ResponseWriter, r *http.Request) {
	fmt.Println("deleting existing and updating new entry")
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	// if r.Body == nil {
	// 	json.NewEncoder(w).Encode("user input is empty")
	// 	return
	// }

	//loop,id remove from slices, add new id
	for index, course := range courses {
		if course.isEmpty() {
			json.NewEncoder(w).Encode("no data in nputed json ")

		}
		if course.Courseid == params["id"] {
			courses = append(courses[:index], courses[index+1:]...) // vardiac to delete existing id in fake db
			var course Course                                       // typing structure for recieveing from r.Body
			_ = json.NewDecoder(r.Body).Decode(&course)             //we need to decode it beacuse we get encoded json
			course.Courseid = params["id"]
			courses = append(courses, course)
			json.NewEncoder(w).Encode(course)
			return
		}

	}
	json.NewEncoder(w).Encode("id you sent is not found")
	return

}

//delete one course

func deleteonecourse(w http.ResponseWriter, r *http.Request) {
	fmt.Println("delete one course from courses")
	w.Header().Set("Content-Type", "application/json")

	params := mux.Vars(r)
	// if r.Body == nil {
	// 	json.NewEncoder(w).Encode("user inout in r.body is empty")
	// }
	// var course Course
	// _ = json.NewDecoder(r.Body).Decode(&course)
	// if course.isEmpty() {
	// 	json.NewEncoder(w).Encode("null id  inside inputted body {}")
	// }
	for index, course := range courses {
		if course.Courseid == params["id"] {
			courses = append(courses[:index], courses[index+1:]...)
			json.NewEncoder(w).Encode(`course id` + params["id"] + ` is removed from fake db`)
			return
			break
		}
	}
	json.NewEncoder(w).Encode(` course id` + params["id"] + ` not found in fake db`)
	return
}

//delete all courses in fake db
func deleteallcourse(w http.ResponseWriter, r *http.Request) {
	fmt.Println("delete all courses from courses")
	w.Header().Set("Content-Type", "application/json")

	// params := mux.Vars(r)
	for index, _ := range courses {
		// if course.Courseid == params["id"] {
		courses = append(courses[:index], courses[index+1:]...)
		json.NewEncoder(w).Encode(`course id {course.Courseid} is removed from fake db`)
		// break
		// }
	}
}
