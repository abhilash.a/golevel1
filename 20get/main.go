package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func main() {
	const myurl = "http://localhost:8000"
	response, err := http.Get(myurl)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	fmt.Println(response.StatusCode)
	fmt.Println(response.ContentLength)

	var responseString strings.Builder
	content, _ := ioutil.ReadAll(response.Body) //string has builder method to write
	// fmt.Println(string(content))  //insted of this  below methoda is used  declare string .builder and  variable type has so many methods
	byteCount, _ := responseString.Write(content) //accept byte of strings and return number oflength
	fmt.Println(byteCount)                        //length
	fmt.Println(responseString.String())
	// responseString.     has soo many method         //givers the thing in myurl"s body

}
