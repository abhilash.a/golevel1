package models

type Response struct {
	Data    interface{}
	Status  int    `json:"status"`
	Message string `json:"message"`
	Error   []ErrorDetail
}
type ErrorDetail struct {
	Response
	ErrorType    string
	ErrorMessage string
}
type ErrorTypeError struct {
}
type ErrorTypeValidation struct {
}
