package handler

import (
	"fmt"
	"jwt/models"
	"jwt/token"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func LoginHandler(context *gin.Context) {
	var loginObj models.LoginRequest
	if err := context.ShouldBindJSON(&loginObj); err != nil {
		var errors []models.ErrorDetail = make([]models.ErrorDetail, 0, 1)
		errors = append(errors, models.ErrorDetail{
			ErrorType:    fmt.Sprintf("%T", err), //ErrorTypeValidation
			ErrorMessage: fmt.Sprintf("%v", err),
		})
		badRequest(context, http.StatusBadRequest, "invalid request", errors)
	}

	// if valid db config user passwors

	//instead odb value hardcodsed belowvar claims = &models.JwtClaims{}
	var claims = &models.JwtClaims{}
	claims.CompanyId = "CompanyId"
	claims.Username = loginObj.UserName
	claims.Roles = []int{1, 2, 3}
	claims.Audience = context.Request.Header.Get("Referer")

	var tokenCreationTime = time.Now().UTC()
	var expirationTime = tokenCreationTime.Add(time.Duration(10) * time.Minute)
	tokenString, err := token.GenerateToken(claims, expirationTime)

	if err != nil {
		badRequest(context, http.StatusBadRequest, "error in generating token", []models.ErrorDetail{
			{
				ErrorType:    fmt.Sprintf("%T", err),
				ErrorMessage: err.Error(),
			},
		})
	}
	ok(context, http.StatusOK, "token created", tokenString)
}
