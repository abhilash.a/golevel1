package token

import (
	models "jwt/models"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	jwtPrivateToken = "SecrteTokenSecrteToken"
	ip              = "192.168.0.107"
)

func GenerateToken(claims *models.JwtClaims, expiration time.Time) (string, error) {
	claims.ExpiresAt = expiration.Unix()
	claims.IssuedAt = time.Now().UTC().Unix()
	claims.Issuer = ip
	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)

	tokenString, err := token.SignedString([]byte(jwtPrivateToken))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}
