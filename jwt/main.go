package main

import (
	"jwt/handler"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.POST("/login", handler.LoginHandler)
	r.Run("localhost:8080")
}
