package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

const url = "http://lco.dev"

func main() {
	response, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	fmt.Printf("type of response is %T \n", response)

	defer response.Body.Close() // respnse public object is body we have to close it compulsory

	content, err := ioutil.ReadAll(response.Body) //reading all response body as div
	if err != nil {
		panic(err)
	}
	fmt.Println(content)
	fmt.Println(string(content))
}
