package main

import "fmt"

func main() {
	abhilash := User{
		Name:  "Abhilash",
		Email: "avlash@gmail.com",
	}
	fmt.Printf("%v \n", abhilash)
	fmt.Printf("%+v \n", abhilash)
	fmt.Printf("the nae is %v  ,the email is %v \n", abhilash.Name, abhilash.Email)
}

type User struct { //capital letter indites tht we intialise it for exporting or accessing it in other place
	Name  string
	Email string
}
