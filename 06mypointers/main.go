package main

import "fmt"

func main() {
	var ptr *int
	fmt.Println("value of ptr is:", ptr)
	fmt.Println("address of ptr is:", &ptr)

	number := 23
	var ptr1 = &number
	fmt.Println("value of ptr is:", ptr1)
	fmt.Println("address of ptr is:", *ptr1)

	*ptr1 = *ptr1 + 2
	fmt.Println("value of ptr1 is:", *ptr1)

}
