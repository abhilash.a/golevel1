package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	v1 := r.Group("v1")
	//Middleware
	v1.Use(func(ctx *gin.Context) {
		authToken := ctx.Request.Header.Get("Authorization")
		if authToken == "" {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"respone": "empty token",
				"User":    ctx.Keys["user"],
			})
			return
		}
		if authToken != "abhilash" {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"respone": "wrong token",
			})
			return
		} else {
			if len(ctx.Keys) == 0 {
				ctx.Keys = make(map[string]interface{})
				ctx.Keys["user"] = authToken
				ctx.Next()
			}

		}

	})
	product := v1.Group("product")
	// user := v1.Group("user")
	product.GET("/info", func(ctx *gin.Context) {
		ctx.IndentedJSON(http.StatusAccepted, gin.H{
			"respone": "hi av lash abhi",
			"user":    ctx.Keys["user"],
		})
	})

	product.GET("/info/message", func(ctx *gin.Context) {
		ctx.IndentedJSON(http.StatusAccepted, gin.H{
			"respone": "Welcome to ooty nice to meet you",
		})
	})
	r.Run(":8080")
}
