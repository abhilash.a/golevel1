package main

import (
	"mime/multipart"
	"net/http"

	"github.com/gin-gonic/gin"
)

type User struct {
	Id     int                   `uri:"id"`
	Name   string                `form:"name"`
	Email  string                `form:"email"`
	Avatar *multipart.FileHeader `form:"avatar" binding:"required"`
} //form is used user passing as queryyparam and GETreqest

//binding data from path parameter  passed by user using verb-PUT
func main() {
	r := gin.Default()
	r.PUT("/user/:id", func(c *gin.Context) {
		var user User
		if err := c.ShouldBind(&user); err != nil { //binding user passed path parameter json to struct model object vaiable
			// fmt.Printf("%+v", err)
			c.String(http.StatusBadRequest, "bad request1")
			return
		}
		if err := c.ShouldBindUri(&user); err != nil { //binding user passed path parameter json to struct model object vaiable
			// fmt.Printf("%+v", err1)
			c.String(http.StatusBadRequest, "bad request2")
			return
		}
		err := c.SaveUploadedFile(user.Avatar, "assets/"+user.Avatar.Filename)
		if err != nil {
			c.String(http.StatusInternalServerError, "internalservererror")
			return
		}
		c.JSON(http.StatusCreated, gin.H{
			"code":     http.StatusCreated,
			"userData": user,
		})
	})
	r.Run(":8080")
}
