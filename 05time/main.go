package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("time pacxkage")
	currentTime := time.Now()
	fmt.Println(currentTime)
	fmt.Println(currentTime.Format("01-02-2006 15:04:05 Mon Jan 2 3PM")) //standard reference time from the document
	createdDate := time.Date(2022, time.July, 30, 07, 07, 0, 0, time.UTC)
	fmt.Println(createdDate)
	fmt.Println(createdDate.Format("01-02-2006 Monday"))
}
