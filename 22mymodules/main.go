package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	greeter()
	r := mux.NewRouter() //mux is the router which supports http
	r.HandleFunc("/", serveHome).Methods("GET")
	log.Fatal(http.ListenAndServe(":4000", r)) //to act our system as server and connect our system server to router
}
func greeter() {
	fmt.Println("go mod")
}
func serveHome(w http.ResponseWriter, r *http.Request) { // here request and response we wrote response according to our need
	w.Write([]byte("<h1>this is response from created server when get request hits</h1>"))
}

//all modules command are written on note or visit go module reference in documwentation
