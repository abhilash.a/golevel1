package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Model struct {
	Id   int    `json:"id" binding:"required"`
	Name string `json:"name" binding:"required"`
} //above binding validate compulsory if two fileds are supplied by user when posting request

func main() {
	r := gin.Default()
	r.GET("", func(c *gin.Context) {
		// c.String(http.StatusOK, "hello")
		c.JSON(http.StatusOK, gin.H{
			"responsedata": "hello",
		})
	})
	r.GET("/:value", func(c *gin.Context) { //takes int nd stinrg as id passing as query in url
		value := c.Param("value")
		c.JSON(http.StatusOK, gin.H{
			"id": value,
			// "name": "abhi",
		})
	})
	r.POST("/add", func(c *gin.Context) { //takes int nd stinrg as id
		var model Model
		if err := c.ShouldBind(&model); err != nil { //compulsory cehck user is inputted two field with post request
			c.JSON(http.StatusBadRequest, gin.H{
				"error": fmt.Sprintf("%v", err),
			})
		} else {
			c.JSON(http.StatusCreated, gin.H{
				"code": http.StatusCreated,
				"data": model,
			})
		}
	})

	r.Run(":8080")
}
