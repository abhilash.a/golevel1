package main

import "fmt"

func main() {
	loginCount := 20
	if loginCount < 10 {
		fmt.Println("less than 10")
	} else if loginCount > 10 {
		fmt.Println("greater than 10")
	} else {
		fmt.Println("equals to 10")
	}

	if 9%2 == 0 {
		fmt.Println("even")
	} else {
		fmt.Println("odd")
	}

	if num = 2; num < 3 { //things from webservices,check that thing with condition
		fmt.Println("less than 3")
	} else {
		fmt.Println("greater  than 3")

	}
}
