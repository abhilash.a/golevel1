package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("enter pizza rating")
	reader := bufio.NewReader(os.Stdin)
	input, _ := reader.ReadString('\n')
	add, err := strconv.ParseFloat(strings.TrimSpace(input), 64) //important strings package
	if err != nil {
		panic(err)
	} else {
		fmt.Printf("%v %T", add+1, add)
	}
}
