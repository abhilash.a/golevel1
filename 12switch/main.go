package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	diceValue := rand.Intn(6 + 1) //range right end is excluded so +1 to get 6
	fmt.Println(diceValue)
	switch diceValue {
	case 1:
		fmt.Println("move 1 spot")
	case 2:
		fmt.Println("move 2 spot")
	case 3:
		fmt.Println("move 3 spot")
	case 4:
		fmt.Println("move 4 spot")
		fallthrough //when it hit it executes currnt and next also
	case 5:
		fmt.Println("move 5 spot")
	case 6:
		fmt.Println("move 6 spot and throw once more")
	default:
		fmt.Println("throw once more")

	}

}
