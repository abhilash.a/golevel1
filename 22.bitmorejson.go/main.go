package main

import (
	"encoding/json"
	"fmt"
)

type course struct { //we are decided no export of struct
	Name     string   `json:"companyname"`
	Price    int      `json:"price"`
	Platform string   `json:"platform"`
	Password string   `json:"-"`              //to avoid password visible in generated in json
	Tags     []string `json:"tags,omitempty"` //omitempty to avoid nill,avoid display numm in generated json
}

func main() {
	fmt.Println("json creation ingonoring password and nill values or encoding json data")
	// encodeJson()
	decodeJson()

}

// func encodeJson() {
// 	courses := []course{
// 		{"Mern Bootcamp", 299, "codeonline.in", "adajshd", []string{"fullstack", "partialstack"}},
// 		{"fdf Bootcamp", 399, "codeonline.in", "adajshd", []string{"fullstack", "partialstack"}},
// 		{"fdf Bootcamp", 399, "codeonline.in", "adajshd", nil}, //nill provides null in generated json
// 	} //importtant another name of struct is an interface
// 	encodedJson, err := json.MarshalIndent(courses, "", "\t") //2nd prefix para:- provides label to each line in generated output json
// 	if err != nil {                                           //3rd tabspace para:=to get tap space to keyvalues insise{}
// 		panic(err)
// 	}
// 	fmt.Printf("%s", encodedJson)
// 	// fmt.Println(encodedJson)
// }

func decodeJson() { //decoding of json from th web
	dataFromWeb := []byte(`{
		"Name": "Mern Bootcamp",
		"Price": 299,
		"Platform": "codeonline.in",
		"Password": "adajshd",
		"Tags": [
				"fullstack",
				"partialstack"
		]
}`)
	checked := json.Valid(dataFromWeb)
	var structureofdatawebvalue course //data format from web has structure .so value of data is strored in this structure
	if checked {
		//actually data having structre from web is put in to similar strucutre or available structure
		json.Unmarshal(dataFromWeb, &structureofdatawebvalue) //2nd para acts as interface,structure nd interface are same here
		fmt.Printf("/n%#v\n\n", structureofdatawebvalue)      //# is used to retrieve value from interface
	} else {
		fmt.Println("json invalid")
	}
	//here below interface is used beacuse field can be slice,int,float,structure etc
	var onlineData map[string]interface{} //data format from web has structure .so value of data is strored in  map allso
	json.Unmarshal(dataFromWeb, &onlineData)
	fmt.Printf("%#v\n", onlineData) //# is used to retrieve value from interface
	for k, v := range onlineData {
		fmt.Printf("key:%v,value:%v,type: %T", k, v, v)
	}

}
