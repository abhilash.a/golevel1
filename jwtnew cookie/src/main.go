package main

import (
	"fmt"
	"jwtnew/src/controller"
	"jwtnew/src/service"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

func main() {
	var loginService service.LoginService = service.StaticLoginService()
	var jwtService service.JWTService = service.JWTAuthService()
	var loginController controller.LoginController = controller.LoginHandler(loginService, jwtService)

	server := gin.New()
	var token string
	// var storeToken string
	server.POST("/login", func(ctx *gin.Context) {
		token = loginController.Login(ctx)
		if token != "" {
			ctx.JSON(http.StatusOK, gin.H{
				"token": token,
			})
			http.SetCookie(ctx.Writer, &http.Cookie{
				Name:  "token",
				Value: token,
				// Expires: service.jwt.StandardClaims.ExpiresAt,
			})
		} else {
			ctx.JSON(http.StatusUnauthorized, nil)
		}
	})
	server.GET("/login/in", func(ctx *gin.Context) {
		// if err := ctx.BindJSON(&storeToken); err != nil {
		// 	return
		// }
		// if storeToken == token {
		// 	ctx.JSON(http.StatusOK, gin.H{
		// 		"user":    token,
		// 		"message": "welcome login",
		// 	})
		// 	tokenString := token
		// 	claims := jwt.MapClaims{}
		// 	token, _ := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		// 		return []byte("service.secret"), nil
		// 	})

		c, err := ctx.Cookie("token")
		if err != nil {
			fmt.Printf("%v", err)

		}
		tokenString := c
		claims := jwt.MapClaims{}
		token, _ := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
			return []byte("service.secret"), nil
		})
		// if err != nil {
		// 	ctx.JSON(http.StatusNoContent, "error with token")
		// }
		ctx.JSON(http.StatusOK, gin.H{
			"user":    token,
			"message": "welcome login",
		})
		// ... error handling

		// do something with decoded claims
		for key, val := range claims {
			fmt.Printf("Key: %v, value: %v\n", key, val)
		}

		// } else {

		// 	ctx.JSON(http.StatusUnauthorized, nil)
		// }

	})
	port := "8080"
	server.Run(":" + port)

}

// if token != "" {
// 	ctx.JSON(http.StatusOK, gin.H{
// 		"user":    token,
// 		"message": "welcome login",
// 	})
// }
