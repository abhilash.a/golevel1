package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

const url = "http://leo.dev"

func main() {
	// readerinput := bufio.NewReader(os.Stdin)
	// fmt.Println("enter rating 1 to 5")
	// convertintofloat, _ := readerinput.ReadString('\n')
	// addnumber, err := strconv.ParseFloat(strings.TrimSpace(convertintofloat), 64)
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println("number+1 is", addnumber+1)

	// newFile, err := os.Create("./abhi1.txt")
	// if err != nil {
	// 	panic(err)
	// }
	// content := "hi avlashabhi"
	// length, err := newFile.WriteString(content)
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println("length of the content is", length)
	// readcreatedFile("./abhi1.txt")
	// func readcreatedFile(name string) {
	// 	byteString, _ := ioutil.ReadFile(name)
	// 	fmt.Println(byteString)
	// 	fmt.Println(string(byteString))
	// }
	response, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	fmt.Println(response)

	fmt.Printf("\n%T\n", response)

	//got res.body then read it
	bytestings, _ := ioutil.ReadAll(response.Body)
	fmt.Println(bytestings)
	fmt.Println(string(bytestings))

}
