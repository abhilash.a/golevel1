package main

import (
	"fmt"
	"net/url"
)

const myurl string = "http://lco.dev:3000/learn?course=python&stream=54242ffd"

func main() {
	fmt.Println("handling an url and creating an url")
	fmt.Println(myurl)
	parameters, err := url.Parse(myurl) //parsing of url into url structure
	if err != nil {
		panic(err)
	}
	fmt.Println("pa", parameters)
	fmt.Println(parameters.Scheme)
	fmt.Println(parameters.Host)
	fmt.Println(parameters.Path)
	// fmt.Println("a", parameters.RawPath)
	fmt.Println(parameters.RawQuery)
	fmt.Println(parameters.Port())

	queryParameters := parameters.Query()     //another method to tetrieve chunks of url as keyvalue pairs
	fmt.Printf("type is %T", queryParameters) //type url.values means key value pairs
	fmt.Println(queryParameters)

	fmt.Println(queryParameters["course"])
	for key, val := range queryParameters {
		fmt.Println(key, val)
	}

	//chunks of url and construct url
	newUrl := &url.URL{ //& is important because we are passing refrence but not copy of url
		Scheme:  "http",
		Host:    "lco.dev",
		Path:    "/tutcss",
		RawPath: "user=hithesh",
	}
	fmt.Println("newString", newUrl)
	stringUrl := newUrl.String()
	fmt.Println(stringUrl) //url is generated control+click to visite url

}
