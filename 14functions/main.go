package main

import "fmt"

func main() {
	fmt.Println(adder(2, 3))
	value, _ := adderPro(2, 3, 4, 4)
	fmt.Println(value)
	value1, message := adderPro(2, 3, 4, 4)
	fmt.Println(value1, message)
}
func adder(value1 int, value2 int) int { //signed type
	return value1 + value2 //functon signature is expecting
}
func adderPro(nums ...int) (int, string) { //varidiac functions it is slice of values thats why we use loop for accessing it
	total := 0
	for _, val := range nums {
		total += val
	}
	return total, "calculated"
}
