package main

import "fmt"

func main() {
	var fruitList = [4]string{}
	fruitList[0] = "apple"
	fruitList[1] = "orange"
	fruitList[3] = "tomato"
	fmt.Println("fruitlist is", fruitList)
	fmt.Println("fruitlist is", len(fruitList)) // have three values but length 4 ,,index 2 space is there

	fruitList[3] = "potato"
	fmt.Println("fruitlist is", fruitList)

	fruitList1 := fruitList //array is not mutable by default
	fruitList1[3] = "grapes"
	fmt.Println("fruitlist1 is", fruitList)
	fmt.Println("fruitlist is", fruitList1)

	fruitList2 := &fruitList
	fruitList2[3] = "grapes2"
	fmt.Println("fruitlist is", fruitList)
	fmt.Println("fruitlist2 is", fruitList2)

	fruitList3 := &fruitList //array can be mutate by adding reference
	fruitList[3] = "grapes3"
	fmt.Println("fruitlist is", fruitList)
	fmt.Println("fruitlist2 is", fruitList3)

	var vegList = [3]string{"potato,cauli,coriander"}
	fmt.Println("fruitlist2 is", vegList) //last space is added vacant element is there

}
