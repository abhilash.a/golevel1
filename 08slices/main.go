package main

import (
	"fmt"
	"sort"
)

func main() {
	var fruitList = []int{432, 331, 321}
	fmt.Println("fruitlist is", fruitList)
	fmt.Printf("%T\n", fruitList) //returns slice

	fruitList = append(fruitList, 765, 655)
	fmt.Println(fruitList)
	fmt.Println((sort.IntsAreSorted(fruitList)))
	sort.Ints(fruitList)
	fmt.Println((sort.IntsAreSorted(fruitList)))
	fmt.Println(fruitList)

	fruitList = append(fruitList[1:3])
	fmt.Println(fruitList)
	fmt.Println(fruitList) //once append is applied on existing slice and then sliced elements is printed and updated and remaing are deleted
	fmt.Println(len(fruitList))

	scores := make([]int, 2)
	scores[0] = 123
	scores[1] = 124
	fmt.Println(scores)
	scores = append(scores, 163, 164)
	fmt.Println(scores)
	scores = append(scores[2:4], 168, 169)
	fmt.Println(scores)
	fmt.Println(scores[2:4])
	fmt.Println(scores)
	scores = append(scores[2:4])
	fmt.Println(scores)

	var courses = []string{"go", "js", "java", "swift", "python", "ruby"}
	var index int = 2
	courses = append(courses[:index], courses[index+1:]...)
	fmt.Println(courses)
	courses = append(courses[:index], courses[index+1:]...)
	fmt.Println(courses)

}
