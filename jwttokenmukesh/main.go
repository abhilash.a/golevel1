package main

import (
	middlewares "jwttoken/authlast"
	"jwttoken/database"

	"jwttoken/controller"

	"github.com/gin-gonic/gin"
)

func main() {
	// Initialize Database
	database.Connect("root:root@tcp(localhost:3306)/jwt_demo?parseTime=true")
	database.Migrate()
	// Initialize Router
	router := initRouter()
	router.Run(":8080")
}
func initRouter() *gin.Engine {
	router := gin.Default()
	api := router.Group("/api")
	{
		api.POST("/token", controller.GenerateToken)
		api.POST("/user/register", controller.RegisterUser)
		secured := api.Group("/secured").Use(middlewares.Auth())
		{
			secured.GET("/ping", controller.Ping)
		}
	}
	return router
}
