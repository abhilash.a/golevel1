package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {
	content := "im abhilash a visit abhi.com" //content to be inserted into file
	file, err := os.Create("abhi.txt")        // ./ is essential  here file havind name abhi.text is generated
	errCall(err)
	length, err := io.WriteString(file, content) //contes content into file
	errCall(err)
	fmt.Println(length)
	readFile("./abhi.txt") //calling function defined below
}
func readFile(fileName string) {
	dataByte, err := ioutil.ReadFile(fileName) //ioutil allows to read and manupulate file that is already created
	errCall(err)
	fmt.Println(dataByte)
	fmt.Println(string(dataByte))

}
func errCall(err error) {
	if err != nil {
		panic(err)
	}
}
