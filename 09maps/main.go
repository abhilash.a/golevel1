package main

import "fmt"

func main() {
	maps := map[string]string{ //maps is mutable by default no need to use reference
		"a": "abhi",
		"b": "lash",
	}
	fmt.Println(maps)
	for key, value := range maps {
		fmt.Println(key, value)
	}

	delete(maps, "a")
	for key, value := range maps {
		fmt.Println("\n", key, value)
	}

	map2 := make(map[int]string)
	map2[1] = "av"
	fmt.Println(map2)

}
