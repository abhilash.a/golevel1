package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/hitheshchoudhary/mongoapi/router"
)

func main() {
	fmt.Println("mongoapi")

	r := router.Router()
	fmt.Println("Server is getting started")
	log.Fatal(http.ListenAndServe(":4000", r))
	fmt.Println("Server is litsenign @port 4000")

}
