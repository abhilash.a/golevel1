package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type Netflix struct {
	Id      primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"` //primitve provide id generted by mongodb,which is of underscore json form, that is bison
	Movie   string             `json:"movie,omitempty"`
	Watched bool               `json:"watched,omitempty"`
}
