package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/hitheshchoudhary/mongoapi/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const connectionString = "mongodb+srv://abhilash_111:filmstar@cluster0.de3f69f.mongodb.net/?retryWrites=true&w=majority"
const dbname = "netflix"
const colname = "watchlist" //collectionname

//importnant with these using values from db

var collection *mongo.Collection

//conntect whith mongodb

func init() { //performs only one time @ start of mongodb connection
	//client option
	clientOption := options.Client().ApplyURI(connectionString)
	//conntect to monngo db
	client, err := mongo.Connect(context.TODO(), clientOption) //contect can be background
	//when a computer is coonected to another machine,context is required or communication between two api's
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("mongo db connection successfull")
	//collection instance
	collection = client.Database(dbname).Collection(colname) // we reached inside database and inside collection
	fmt.Println("collection instance is ready")

}

//mongo db helper method

//insert a record

func insertOneMovie(movie model.Netflix) { //movie inside created Netflix structre

	inserted, err := collection.InsertOne(context.Background(), movie)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("inserted")
	fmt.Println("inserted one movie in db with id:", inserted.InsertedID)
}

//update one record in mongo db

func updateOneRecord(movieId string) {
	id, _ := primitive.ObjectIDFromHex(movieId)       //convert movieid string into objectid which is acceptable by mongodb
	filter := bson.M{"_id": id}                       //any update in mongo use bson.m or bson.d
	update := bson.M{"$set": bson.M{"watched": true}} //$set is used to set value in a column isnisde db
	//contect.Context it return err para also
	result, err := collection.UpdateOne(context.Background(), filter, update) //if we want to perform any operation in db ,context is required and edit values required
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("modified count:", result.ModifiedCount)
}

//delete one record in mongo db

func deleteOneRecord(movieId string) {
	id, _ := primitive.ObjectIDFromHex(movieId)
	filter := bson.M{"_id": id}
	result, err := collection.DeleteOne(context.Background(), filter)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("the number of delete count is", result.DeletedCount)
}

//delete all recor in mongo db
func deleteAllRecord() int64 {
	result, err := collection.DeleteMany(context.Background(), bson.D{{}}) //{} delete all record in mongo,becz value is not specified
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("the number of delete count is", result.DeletedCount)
	return result.DeletedCount
}

//to tget all records from mongodb

func getAllMovies() []primitive.D {
	cursor, err := collection.Find(context.Background(), bson.D{{}}) //cursor is gigantic object of mongo db which hasvalues to loop through
	if err != nil {
		log.Fatal(err)
	}
	var movies []primitive.D
	for cursor.Next(context.Background()) { // loop through mongos object which hold set of values  similar to linklist
		var movie bson.D // strcutr to recieve datas from mongodb
		err := cursor.Decode(&movie)
		if err != nil {
			log.Fatal(err)
		}
		movies = append(movies, movie)

	}
	defer cursor.Close(context.Background())
	return movies
}

//mongo db helpers are all done
//ActualControllers  -in another file

func GetMyAllMovies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "x-www-from-urlencode")
	allMovies := getAllMovies()
	json.NewEncoder(w).Encode(allMovies)
}

func CreateMovie(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "x-www-from-urlencode")
	w.Header().Set("Allow-Control-Allow-Methods", "POST")
	var movie model.Netflix
	json.NewDecoder(r.Body).Decode(&movie)
	insertOneMovie(movie)
	json.NewEncoder(w).Encode(movie)

}

func MarkWatched(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "x-www-from-urlencode")
	w.Header().Set("Allow-Control-Allow-Methods", "PUT")
	params := mux.Vars(r)
	updateOneRecord(params["id"])
	json.NewEncoder(w).Encode(params["id"])

}

//delete movie
func DeleteAMovie(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "x-www-from-urlencode")
	w.Header().Set("Allow-Control-Allow-Methods", "DELETE")
	params := mux.Vars(r)
	deleteOneRecord(params["id"])
	json.NewEncoder(w).Encode(params["id"])

}

func DeleteAllMovie(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "x-www-from-urlencode")
	w.Header().Set("Allow-Control-Allow-Methods", "DELETE")
	count := deleteAllRecord()
	json.NewEncoder(w).Encode(count)

}
