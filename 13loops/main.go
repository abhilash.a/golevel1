package main

import "fmt"

func main() {
	days := []string{"monday", "tuesday", "wednesday"}
	for d := 0; d < len(days); d++ {
		fmt.Println(days[d])
	}

	for d, day := range days {
		fmt.Printf("index:%v,day:%v\n", d, day)
	}
	for _, day := range days {
		fmt.Println(day)
	}

	score := 10
	for score < 20 {

		// if score == 15 {
		// 	break          // break from entrire loop
		// }

		if score == 12 {
			goto stat //break from this loop and  execute go to
		}

		if score == 15 {
			score++ //skip that 15 only but continue upto 20
			continue

		}

		fmt.Println(score)
		score++

	}
stat:
	fmt.Println("hi abhi")
}
