package main

import "fmt"

const Nconstant string = "abhilash" //public
// conta:="abhi"  is not allowed
// var conta string = "abhi"

func main() {
	var number uint8 = 255
	var number2 uint16 = 256
	fmt.Printf("%v %T %v %T", number, number, number2, number2)

	var decimal float32 = 255.54654654
	var ndecimal float64 = 256.54654654
	fmt.Printf("\n%v %T %v %T", decimal, decimal, ndecimal, ndecimal)

	var isLogged = true
	fmt.Printf("\n%v %T", isLogged, isLogged)

	const constant string = "abhi"
	fmt.Printf("\n%v %T", constant, constant)

	fmt.Printf("\n%v %T", Nconstant, Nconstant)

	fmt.Println(`dadadas
	dadasdas
	dasdad`)

}
