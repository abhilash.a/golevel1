package main

import "fmt"

func main() {
	abhilash := User{
		Name:  "Abhilash",
		Email: "avlash@gmail.com",
	}
	fmt.Printf("%v \n", abhilash)
	fmt.Printf("%+v \n", abhilash)
	fmt.Printf("the nae is %v  ,the email is %v \n", abhilash.Name, abhilash.Email)
	abhilash.getName()
	abhilash.editEmail()
	fmt.Printf("the nae is %v  ,the email is %v \n", abhilash.Name, abhilash.Email)
} // here value of email is not edited still we manuulated email value ,copy of struct obj is passed into function

type User struct { //capital letter indites tht we intialise it for exporting or accessing it in other place
	Name  string
	Email string
}

func (u User) getName() {
	fmt.Println(u.Name)

}
func (u User) editEmail() {
	u.Email = "ab@gmail.com"
	fmt.Println(u.Email)
}
