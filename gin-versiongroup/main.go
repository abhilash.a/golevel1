package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type User struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
} //form is used user passing as queryyparam and GETreqest

//binding data from query string passed user using verb-GET
func main() {
	r := gin.Default()
	v1 := r.Group("v1")
	{ //Middleware for token Authorization before executting the requested route
		v1.Use(func(c *gin.Context) {
			authHeader := c.Request.Header.Get("Authorization") //to pass Authorization  token  with get request
			if authHeader == "" {
				c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
					"message": "empty token",
				})
				return
			}
			if authHeader != "abc" {
				c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
					"message": "invalid token",
				})
				return
			} else {
				if len(c.Keys) == 0 {
					c.Keys = make(map[string]interface{})
				}
				c.Keys["user"] = authHeader
				c.Next()
			}

		})
		product := v1.Group("product")
		{
			product.GET(":id", func(c *gin.Context) {
				c.JSON(http.StatusOK, gin.H{
					"user": c.Keys["user"],
					"productData": []User{
						User{
							Id:   1,
							Name: "afin",
						},
						User{
							Id:   2,
							Name: "athira",
						},
					},
				})
			})
		}
		user := v1.Group("user")
		{
			user.GET("", func(c *gin.Context) {

				c.JSON(http.StatusCreated, gin.H{
					"code": http.StatusCreated,
					"userData1": []User{
						User{
							Id:   1,
							Name: "assim",
						},
						User{
							Id:   2,
							Name: "assimdgsg",
						},
					},
				})
			})
			user.GET(":id", func(c *gin.Context) {
				id := c.Param("id")
				c.JSON(http.StatusOK, gin.H{
					"queryparambyuser": id,
					"userdata2": User{
						Id:   5,
						Name: "donkey",
					},
				})
			})
		}
	}

	v2 := r.Group("v2")
	{
		product := v2.Group("product")
		{
			product.GET(":id", func(c *gin.Context) {
				c.JSON(http.StatusOK, gin.H{
					"productDatav2": []User{
						User{
							Id:   1,
							Name: "afin",
						},
						User{
							Id:   2,
							Name: "athira",
						},
					},
				})
			})
		}
		user := v2.Group("user")
		{
			user.GET("", func(c *gin.Context) {

				c.JSON(http.StatusCreated, gin.H{
					"code": http.StatusCreated,
					"userData1v2": []User{
						User{
							Id:   1,
							Name: "assim",
						},
						User{
							Id:   2,
							Name: "assimdgsg",
						},
					},
				})
			})
			user.GET(":id", func(c *gin.Context) {
				id := c.Param("id")
				c.JSON(http.StatusOK, gin.H{
					"queryparambyuserv2": id,
					"userdata2": User{
						Id:   5,
						Name: "donkey",
					},
				})
			})
		}
	}

	r.Run(":8080")
}
