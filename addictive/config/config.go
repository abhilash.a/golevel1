package config

import (
	"fmt"

	"github.com/spf13/viper"
)

func init() {
	viper.SetDefault("appname", "addictive") //set default  key value   if key value is not passed by env file,flag,remote
	ViperLoad()
}

func ViperLoad() {
	viper.SetConfigName("config") //name of config file
	viper.SetConfigType("yml")    //extension of config file
	viper.AddConfigPath("config") //path of config file placed from root mainfn directory
	// workingdir, _ := os.Getwd()  //instead of  config file path  ,above line
	// if err != nil {
	// 	logger.Error(err)
	// }
	// viper.SetConfigFile(workingdir + "/config/config.yml")

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
}
func ViperValue() interface{} {
	val := viper.Get("db")
	return val

}
func KeyVal() map[string]string {
	mapp := viper.GetStringMapString("db")
	return mapp
}
func GetKey() []string {
	keys := viper.AllKeys()
	return keys
}
