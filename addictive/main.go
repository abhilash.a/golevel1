package main

import (
	"addictive/config"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.GET("/viper", func(ctx *gin.Context) {
		ctx.JSON(200, gin.H{
			"viper":     config.ViperValue(),
			"keyvalues": config.KeyVal(),
			"keys":      config.GetKey(),
		})
	})
	router.Run(":8080")
}
