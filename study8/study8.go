// package main  //callbyreference
// import "fmt"
// func main(){
// 	a:=5
// 	b:=6
// 	fmt.Println("a nd b before swap",a,b)
// 	swap(&a,&b)
// 	fmt.Println("a nd b before swap",a,b)
// }
// func swap(x *int,y *int){
// temp:=*x
// *x=*y
// *y=temp
// }

package main  //call by value
import "fmt"
func main(){
	a:=5
	b:=6
	fmt.Println("a nd b before swap",a,b)
	swap(a,b)
	fmt.Println("a nd b after swap",a,b)
	fmt.Println(swap(a,b))
	fmt.Println("a nd b after swap",a,b)
}
func swap(x int,y int)(int,int){
temp:=x
x=y
y=temp
return x,y
}