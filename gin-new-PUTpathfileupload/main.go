package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type User struct {
	Id    int    `uri:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
} //form is used user passing as queryyparam and GETreqest

//binding data from path parameter  passed by user using verb-PUT
func main() {
	r := gin.Default()
	r.PUT("/user/:id", func(c *gin.Context) {
		var user User
		if err := c.ShouldBindUri(&user); err != nil { //binding user passed path parameter json to struct model object vaiable
			fmt.Printf("%+v", err)
		} else {
			fmt.Printf("userobj %+v", user)
		}
		if err1 := c.ShouldBindJSON(&user); err1 != nil { //binding user passed path parameter json to struct model object vaiable
			fmt.Printf("%+v", err1)
		} else {
			fmt.Printf(" userobj1 %+v", user)
		}
		c.JSON(http.StatusCreated, gin.H{
			"code":     http.StatusCreated,
			"userData": user,
		})
	})
	r.Run(":8080")
}
